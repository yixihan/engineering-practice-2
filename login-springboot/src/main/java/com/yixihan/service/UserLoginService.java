package com.yixihan.service;

import com.yixihan.pojo.UserLogin;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
public interface UserLoginService extends IService<UserLogin> {

}
