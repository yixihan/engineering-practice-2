package com.yixihan.service.impl;

import com.yixihan.pojo.ManageLogin;
import com.yixihan.mapper.ManageLoginMapper;
import com.yixihan.service.ManageLoginService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@Service
public class ManageLoginServiceImpl extends ServiceImpl<ManageLoginMapper, ManageLogin> implements ManageLoginService {

}
