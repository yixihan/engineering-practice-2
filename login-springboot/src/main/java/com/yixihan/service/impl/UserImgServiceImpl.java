package com.yixihan.service.impl;

import com.yixihan.pojo.UserImg;
import com.yixihan.mapper.UserImgMapper;
import com.yixihan.service.UserImgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@Service
public class UserImgServiceImpl extends ServiceImpl<UserImgMapper, UserImg> implements UserImgService {

}
