package com.yixihan.service.impl;

import com.yixihan.pojo.ManageInfo;
import com.yixihan.mapper.ManageInfoMapper;
import com.yixihan.service.ManageInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@Service
public class ManageInfoServiceImpl extends ServiceImpl<ManageInfoMapper, ManageInfo> implements ManageInfoService {

}
