package com.yixihan.service.impl;

import com.yixihan.pojo.UserInfo;
import com.yixihan.mapper.UserInfoMapper;
import com.yixihan.service.UserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

}
