package com.yixihan.service.impl;

import com.yixihan.pojo.SuperLogin;
import com.yixihan.mapper.SuperLoginMapper;
import com.yixihan.service.SuperLoginService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@Service
public class SuperLoginServiceImpl extends ServiceImpl<SuperLoginMapper, SuperLogin> implements SuperLoginService {

}
