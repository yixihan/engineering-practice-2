package com.yixihan.service.impl;

import com.yixihan.pojo.Log;
import com.yixihan.mapper.LogMapper;
import com.yixihan.service.LogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@Service
public class LogServiceImpl extends ServiceImpl<LogMapper, Log> implements LogService {

}
