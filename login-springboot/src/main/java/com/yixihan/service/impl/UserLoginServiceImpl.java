package com.yixihan.service.impl;

import com.yixihan.pojo.UserLogin;
import com.yixihan.mapper.UserLoginMapper;
import com.yixihan.service.UserLoginService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@Service
public class UserLoginServiceImpl extends ServiceImpl<UserLoginMapper, UserLogin> implements UserLoginService {

}
