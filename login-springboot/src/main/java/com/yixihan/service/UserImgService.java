package com.yixihan.service;

import com.yixihan.pojo.UserImg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
public interface UserImgService extends IService<UserImg> {

}
