package com.yixihan.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yixihan.commons.Login;
import com.yixihan.commons.Result;
import com.yixihan.pojo.SuperLogin;
import com.yixihan.service.impl.SuperLoginServiceImpl;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@RestController
@RequestMapping("/super-login")
public class SuperLoginController {

    @Autowired
    private SuperLoginServiceImpl login;

    @ApiOperation(value = "super用户登录")
    @PostMapping("/login")
    public Result Login (Login superLogin) {
        QueryWrapper<SuperLogin> wrapper = new QueryWrapper<>();

        wrapper
                .eq("Super_name",superLogin.getName())
                .eq("Super_password",superLogin.getPassword());

       if (login.getOne(wrapper) != null) {
           return Result.success(0,"登录成功",superLogin.getName());
       }
       else {
           return Result.fail("登录失败");
       }
    }

    @ApiOperation(value = "添加super用户")
    @PostMapping("/add")
    public Result add (SuperLogin superLogin) {
        QueryWrapper<SuperLogin> wrapper = new QueryWrapper<>();

        wrapper.eq("Super_name",superLogin.getSuperName());

        if (login.getOne(wrapper) == null) {
            login.save(superLogin);
            return Result.success(0,"注册成功",superLogin);
        }
        else {
            return Result.fail(1,"该用户名已存在");
        }
    }

    @ApiOperation(value = "删除super用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="ID",required=true)
    })
    @PostMapping("/remove")
    public Result remove (@RequestParam int id) {
        boolean b = login.removeById(id);

        if(b) {
            return Result.success(0,"删除成功");
        }
        else {
            return Result.fail("删除失败");
        }
    }

    @ApiOperation(value = "更新super用户")
    @PostMapping("/update")
    public Result update(SuperLogin superLogin) {
        boolean b = login.updateById(superLogin);

        if (b) {
            return Result.success(0,"修改成功");
        }
        else {
            return Result.fail("修改失败");
        }
    }

    @ApiOperation(value = "查询所有super用户")
    @GetMapping("/list")
    public Result list () {
        return Result.success(0,"查询成功",login.list(null));
    }

    @ApiOperation(value = "根据ID查询super用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="ID",required=true)
    })
    @GetMapping("/queryById")
    public Result queryById(@RequestParam int id) {
        SuperLogin superLogin = login.getById(id);

        if (superLogin == null) {
            return Result.fail("无此用户");
        }
        else {
            return Result.success(0,"查询成功",superLogin);
        }
    }

    @ApiOperation(value = "根据Name查询super用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name="name",value="用户名",required=true)
    })
    @GetMapping("/queryByName")
    public Result queryByName(String name) {
        QueryWrapper<SuperLogin> wrapper = new QueryWrapper<>();

        wrapper.eq("Super_name",name);

        SuperLogin one = login.getOne(wrapper);

        if (one == null) {
            return Result.fail("无此用户");
        }
        else {
            return Result.success(0,"查询成功",one);
        }
    }

}
