package com.yixihan.controller;

import com.yixihan.commons.Result;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.internet.MimeMessage;
import java.util.Random;

@RestController
@RequestMapping("/common")
public class CommonController {

    @Autowired
    private JavaMailSender mailSender;

    @ApiOperation(value = "发送邮件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "email",value = "邮箱",required = true)
    })
    @RequestMapping("/send")
    public Result send (@RequestParam String email) throws javax.mail.MessagingException {
        Random r = new Random();

        int min = 10000;
        int max = 99999;

        String rs = String.valueOf(r.nextInt(max-min + 1) + min);

        //一个复杂的文件
        MimeMessage mailMessage = mailSender.createMimeMessage();

        //组装
        MimeMessageHelper helper = new MimeMessageHelper(mailMessage,true,"utf-8");

        helper.setSubject("你好");
        helper.setText("<h1 style='color:red'>验证码为" + rs + "</h1>",true);


        //收件人
        helper.setTo(email);
        //发件人
        helper.setFrom("3113788997@qq.com");


        mailSender.send(mailMessage);

        return Result.success(0,"发送成功",rs);
    }


}
