package com.yixihan.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yixihan.commons.Result;
import com.yixihan.pojo.UserImg;
import com.yixihan.pojo.UserInfo;
import com.yixihan.service.impl.UserImgServiceImpl;
import com.yixihan.service.impl.UserInfoServiceImpl;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@RestController
@RequestMapping("/user-img")
public class UserImgController {

    @Autowired
    private UserImgServiceImpl img;

    @Autowired
    private UserInfoServiceImpl info;

    @ApiOperation(value = "文件上传接口", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="ID",required=true),
            @ApiImplicitParam(name="name",value="上传的照片类型(User_operation,User_Tax,User_gruop,User_coal,User_bank,User_JP)",required=true),
    })
    @PostMapping(value = "/uploadById", headers = "content-type=multipart/form-data")
    public Result uploadById(
            int id,
            String name,
            @ApiParam(value = "文件", required = true) @RequestPart("file") MultipartFile file,
            HttpServletRequest request
    ) throws IOException {
        //获取文件名 ： file.getOriginalFilename();
        String filename = file.getOriginalFilename();

        //上传路径保存设置  UUID
        String path = request.getServletContext().getRealPath("/upload");

        File realPath = new File(path);
        if (!realPath.exists()) {
            realPath.mkdir();
        }
        System.out.println("上传文件保存地址 =>" + realPath);

        //通过 CommonsMultipartFile 的方法直接写文件 （注意这个时候）
        file.transferTo(new File(realPath + "/" + file.getOriginalFilename()));

        //将路径写入数据库
        UserImg userImg = img.getById(id);

        switch (name) {
            case "User_operation":
                userImg.setUserOperationUrl(String.valueOf(realPath));
                userImg.setUserOperationName(filename);
                break;
            case "User_Tax":
                userImg.setUserTaxUrl(String.valueOf(realPath));
                userImg.setUserTaxName(filename);
                break;
            case "User_gruop":
                userImg.setUserGruopUrl(String.valueOf(realPath));
                userImg.setUserGruopName(filename);
                break;
            case "User_coal":
                userImg.setUserCoalUrl(String.valueOf(realPath));
                userImg.setUserCoalName(filename);
                break;
            case "User_bank":
                userImg.setUserBankUrl(String.valueOf(realPath));
                userImg.setUserBankName(filename);
                break;
            case "User_JP":
                userImg.setUserJpUrl(String.valueOf(realPath));
                userImg.setUserJpName(filename);
                break;
            default:
                return Result.fail("上传失败");
        }

        img.updateById(userImg);

        return Result.success(0,"上传成功",realPath);
    }

    @ApiOperation(value = "文件上传接口", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name="username",value="用户名",required=true),
            @ApiImplicitParam(name="name",value="上传的照片类型(User_operation,User_Tax,User_gruop,User_coal,User_bank,User_JP)",required=true),
    })
    @PostMapping(value = "/uploadByName", headers = "content-type=multipart/form-data")
    public Result uploadByName(
            String username,
            String name,
            @ApiParam(value = "文件", required = true) @RequestPart("file") MultipartFile file,
            HttpServletRequest request
    ) throws IOException {
        //获取文件名 ： file.getOriginalFilename();
        String filename = file.getOriginalFilename();

        //上传路径保存设置  UUID
        String path = request.getServletContext().getRealPath("/upload");

        File realPath = new File(path);
        if (!realPath.exists()) {
            realPath.mkdir();
        }
        System.out.println("上传文件保存地址 =>" + realPath);

        //通过 CommonsMultipartFile 的方法直接写文件 （注意这个时候）
        file.transferTo(new File(realPath + "/" + file.getOriginalFilename()));

        //将路径写入数据库
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("User_name",username);
        UserInfo one = info.getOne(wrapper);
        UserImg userImg = img.getById(one.getUserId());

        switch (name) {
            case "User_operation":
                userImg.setUserOperationUrl(String.valueOf(realPath));
                userImg.setUserOperationName(filename);
                break;
            case "User_Tax":
                userImg.setUserTaxUrl(String.valueOf(realPath));
                userImg.setUserTaxName(filename);
                break;
            case "User_gruop":
                userImg.setUserGruopUrl(String.valueOf(realPath));
                userImg.setUserGruopName(filename);
                break;
            case "User_coal":
                userImg.setUserCoalUrl(String.valueOf(realPath));
                userImg.setUserCoalName(filename);
                break;
            case "User_bank":
                userImg.setUserBankUrl(String.valueOf(realPath));
                userImg.setUserBankName(filename);
                break;
            case "User_JP":
                userImg.setUserJpUrl(String.valueOf(realPath));
                userImg.setUserJpName(filename);
                break;
            default:
                return Result.fail("上传失败");
        }

        img.updateById(userImg);

        return Result.success(0,"上传成功",realPath);
    }


    @ApiOperation(value = "文件下载接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="ID",required=true),
            @ApiImplicitParam(name="name",value="下载的照片类型(User_operation,User_Tax,User_gruop,User_coal,User_bank,User_JP)",required=true),
    })
    @GetMapping("/downloadById")
    public Result downloadById(
            int id,
            String name,
            HttpServletResponse response
    ) throws IOException {
        //将文件地址及名字从数据库中读出
        UserImg userImg = img.getById(id);

        System.out.println("文件url=>"+userImg.getUserCoalUrl());

        //要下载的图片地址
        String Path = null;
        String fileName = null;

        switch (name) {
            case "User_operation":
                Path = userImg.getUserOperationUrl();
                fileName = userImg.getUserOperationName();
                break;
            case "User_Tax":
                Path = userImg.getUserTaxUrl();
                fileName = userImg.getUserTaxName();
                break;
            case "User_gruop":
                Path = userImg.getUserGruopUrl();
                fileName = userImg.getUserGruopName();
                break;
            case "User_coal":
                Path = userImg.getUserCoalUrl();
                fileName = userImg.getUserCoalName();
                break;
            case "User_bank":
                Path = userImg.getUserBankUrl();
                fileName = userImg.getUserBankName();
                break;
            case "User_JP":
                Path = userImg.getUserJpUrl();
                fileName = userImg.getUserJpName();
                break;
            default:
                break;
        }

        if ((Path == null) && (fileName == null)) {
            return Result.fail("文件获取失败");
        }
        //1.设置 response 响应头
        response.reset();               //设置页面不缓存，清空buffer
        response.setCharacterEncoding("UTF-8");     //字符编码
        response.setContentType("multipart/form-data");     //二进制传输数据

        //设置响应头
        response.setHeader("Content-Disposition",
                "attachment;fileName=" + URLEncoder.encode(fileName, "UTF-8"));

        File file = new File(Path, fileName);
        //2.读取文件---输入流
        FileInputStream inputStream = new FileInputStream(file);

        //3.写出文件---输出流
        ServletOutputStream outputStream = response.getOutputStream();

        //4.执行写出操作
        int index;
        byte[] buffer = new byte[1024];
        while ((index = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, index);
            outputStream.flush();
        }
        outputStream.close();
        inputStream.close();
        return Result.success(0,"文件获取成功");
    }

    @ApiOperation(value = "文件下载接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name="username",value="用户名",required=true),
            @ApiImplicitParam(name="name",value="下载的照片类型(User_operation,User_Tax,User_gruop,User_coal,User_bank,User_JP)",required=true),
    })
    @GetMapping("/downloadByName")
    public Result downloadByName(
            String username,
            String name,
            HttpServletResponse response
    ) throws IOException {
        //将文件地址及名字从数据库中读出
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("User_name",username);
        UserInfo one = info.getOne(wrapper);
        UserImg userImg = img.getById(one.getUserId());

        System.out.println("文件url=>"+userImg.getUserCoalUrl());

        //要下载的图片地址
        String Path = null;
        String fileName = null;

        switch (name) {
            case "User_operation":
                Path = userImg.getUserOperationUrl();
                fileName = userImg.getUserOperationName();
                break;
            case "User_Tax":
                Path = userImg.getUserTaxUrl();
                fileName = userImg.getUserTaxName();
                break;
            case "User_gruop":
                Path = userImg.getUserGruopUrl();
                fileName = userImg.getUserGruopName();
                break;
            case "User_coal":
                Path = userImg.getUserCoalUrl();
                fileName = userImg.getUserCoalName();
                break;
            case "User_bank":
                Path = userImg.getUserBankUrl();
                fileName = userImg.getUserBankName();
                break;
            case "User_JP":
                Path = userImg.getUserJpUrl();
                fileName = userImg.getUserJpName();
                break;
            default:
                break;
        }

        if ((Path == null) && (fileName == null)) {
            return Result.fail("文件获取失败");
        }
        //1.设置 response 响应头
        response.reset();               //设置页面不缓存，清空buffer
        response.setCharacterEncoding("UTF-8");     //字符编码
        response.setContentType("multipart/form-data");     //二进制传输数据

        //设置响应头
        response.setHeader("Content-Disposition",
                "attachment;fileName=" + URLEncoder.encode(fileName, "UTF-8"));

        File file = new File(Path, fileName);
        //2.读取文件---输入流
        FileInputStream inputStream = new FileInputStream(file);

        //3.写出文件---输出流
        ServletOutputStream outputStream = response.getOutputStream();

        //4.执行写出操作
        int index;
        byte[] buffer = new byte[1024];
        while ((index = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, index);
            outputStream.flush();
        }
        outputStream.close();
        inputStream.close();
        return Result.success(0,"文件获取成功");
    }

}
