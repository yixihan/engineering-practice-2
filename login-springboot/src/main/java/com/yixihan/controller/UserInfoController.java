package com.yixihan.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yixihan.commons.Result;
import com.yixihan.pojo.*;
import com.yixihan.service.impl.UserImgServiceImpl;
import com.yixihan.service.impl.UserInfoServiceImpl;
import com.yixihan.service.impl.UserLoginServiceImpl;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@RestController
@RequestMapping("/user-info")
public class UserInfoController {

    @Autowired
    private UserImgServiceImpl img;

    @Autowired
    private UserLoginServiceImpl login;

    @Autowired
    private UserInfoServiceImpl info;

    @ApiOperation(value = "添加用户(第一次注册)")
    @PostMapping("/add1")
    public Result add1 (UserInfo user) {
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();

        wrapper.eq("User_name",user.getUserName());

        if (info.getOne(wrapper) == null) {
            boolean a = info.save(user);
            UserLogin userLogin = new UserLogin(user.getUserId(),user.getUserPassword());
            boolean b = login.save(userLogin);
            UserImg userImg = new UserImg(user.getUserId());
            boolean c = img.save(userImg);

            if(a && b && c) {
                return Result.success(0, "注册成功", user.getUserType() + "," + user.getUserName());
            }
            else {
                return Result.fail(1,"注册失败");
            }
        }
        else if (info.getOne(wrapper) != null){
            return Result.fail(2,"该用户名已存在");
        }
        else {
            return Result.fail(1,"注册失败");
        }
    }


    @ApiOperation(value = "添加用户(第二次注册,通过name)")
    @PostMapping("/add2ByName")
    public Result add2ByName (UserInfo user) {
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();

        wrapper.eq("User_name",user.getUserName());
        UserInfo one = info.getOne(wrapper);
        if (one != null) {
            user.setUserId(one.getUserId());
            boolean a = info.updateById(user);

            if(a) {
                return Result.success(0, "第二次注册成功", info.getById(user.getUserId()));
            }
            else {
                return Result.fail(1,"注册失败");
            }
        }
        else {
            return Result.fail(1,"注册失败");
        }
    }


    @ApiOperation(value = "添加用户(第二次注册,通过id)")
    @PostMapping("/add2ById")
    public Result add2ById (UserInfo user) {

        if (info.getById(user.getUserId()) == null) {
            boolean a = info.updateById(user);

            if(a) {
                return Result.success(0, "第二次注册成功", info.getById(user.getUserId()));
            }
            else {
                return Result.fail(1,"注册失败");
            }
        }
        else {
            return Result.fail(1,"注册失败");
        }
    }


    @ApiOperation(value = "删除用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="该用户的ID",required=true)
    })
    @PostMapping("/remove")
    public Result remove(@RequestParam int id) {
        boolean a = info.removeById(id);
        boolean b = login.removeById(id);
        boolean c = img.removeById(id);


        if (a && b && c) {
            return Result.success(0,"删除成功");
        }
        else  {
            return Result.fail("删除失败");
        }
    }


    @ApiOperation(value = "更新用户资料")
    @PostMapping("/update")
    public Result update(UserInfo userInfo) {
        boolean b = info.updateById(userInfo);

        if (b) {
            return Result.success(0,"修改成功");
        }
        else {
            return Result.fail("修改失败");
        }
    }


    @ApiOperation(value = "查询所有未审核的用户")
    @GetMapping("/list2")
    public Result list2() {
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("Login_type",0);
        return Result.success(0,"查询成功",info.list(wrapper));
    }


    @ApiOperation(value = "查询所有审核通过的用户")
    @GetMapping("/list3")
    public Result list3() {
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("Login_type",1);
        return Result.success(0,"查询成功",info.list(wrapper));
    }


    @ApiOperation(value = "查询所有审核未通过的用户")
    @GetMapping("/list4")
    public Result list4() {
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("Login_type",2);
        return Result.success(0,"查询成功",info.list(wrapper));
    }


    @ApiOperation(value = "查询所有用户")
    @GetMapping("/list")
    public Result list() {
        return Result.success(0,"查询成功",info.list(null));
    }


    @ApiOperation(value = "根据ID查询用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="该用户的ID",required=true)
    })
    @GetMapping("/queryById")
    public Result queryById(@RequestParam int id) {
        UserInfo one = info.getById(id);

        if (one == null) {
            return Result.fail("无此用户");
        }
        else {
            return Result.success(0,"查询成功",one);
        }
    }


    @ApiOperation(value = "根据ID查询用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name="name",value="用户名",required=true)
    })
    @GetMapping("/queryByName")
    public Result queryByName(@RequestParam String name) {
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();

        wrapper.eq("User_name",name);

        UserInfo one = info.getOne(wrapper);

        if (one == null) {
            return Result.fail("无此用户");
        }
        else {
            return Result.success(0,"查询成功",one);
        }
    }


    @ApiOperation(value = "根据ID查询用户是否审核通过")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="ID",required=true)
    })
    @GetMapping("/examineById")
    public Result examineById(int id) {
        UserInfo info = this.info.getById(id);

        return getResult(info);
    }


    @ApiOperation(value = "根据name查询用户是否审核通过")
    @ApiImplicitParams({
            @ApiImplicitParam(name="name",value="用户名",required=true)
    })
    @GetMapping("/examineByName")
    public Result examineByName(String name) {
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();

        wrapper.eq("User_name",name);

        UserInfo info = this.info.getOne(wrapper);

        return getResult(info);
    }


    private Result getResult(UserInfo info) {
        if (info.getLoginType() == 0) {
            return Result.success(2,"用户正在审核",info);
        }
        else if (info.getLoginType() == 1) {
            return Result.success(0,"审核通过",info);
        }
        else if (info.getLoginType() == 2) {
            return Result.success(3,"审核未通过",info);
        }
        else {
            return Result.fail("出现错误");
        }
    }


}
