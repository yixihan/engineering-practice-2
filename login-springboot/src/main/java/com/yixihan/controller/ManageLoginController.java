package com.yixihan.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yixihan.commons.Result;
import com.yixihan.commons.Login;
import com.yixihan.pojo.ManageInfo;
import com.yixihan.pojo.ManageLogin;
import com.yixihan.service.impl.ManageInfoServiceImpl;
import com.yixihan.service.impl.ManageLoginServiceImpl;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@RestController
@RequestMapping("/manage-login")
public class ManageLoginController {

    @Autowired
    private ManageLoginServiceImpl login;

    @Autowired
    private ManageInfoServiceImpl info;

    @ApiOperation(value = "manage用户登录")
    @PostMapping("/login")
    public Result Login (Login manage) {
        QueryWrapper<ManageInfo> wrapper = new QueryWrapper<>();

        wrapper
                .eq("Manage_name",manage.getName())
                .eq("Manage_password",manage.getPassword());

        if(info.getOne(wrapper) != null) {

            return Result.success(0,"登录成功",manage.getName());
        }
        else {
            return Result.fail("登录失败");
        }
    }

    @PostMapping("/find")
    @ApiOperation(value = "根据email找到该用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name="email",value="邮箱",required=true)
    })
    public Result FindManage(@RequestParam String email) {
        QueryWrapper<ManageInfo> wrapper = new QueryWrapper<>();

        wrapper.eq("Manage_email",email);

        if(info.getOne(wrapper) != null) {
            ManageInfo manageInfo = info.getOne(wrapper);

            return Result.success(0,"账号查找成功",manageInfo.getManageId());
        }
        else {
            return Result.fail("账号查找失败");
        }
    }

    @ApiOperation(value = "修改manage密码")
    @PostMapping("/change")
    public Result ChangePassword(ManageLogin manage) {
        boolean a = login.updateById(manage);


        ManageInfo manageInfo = new ManageInfo(manage.getManagePassword());
        QueryWrapper<ManageInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("Manage_id",manage.getManageId());

        boolean b = info.update(manageInfo, wrapper);

        if (a && b) {
            return Result.success(0,"修改密码成功");
        }
        else {
            return Result.fail("修改密码失败");
        }
    }

}
