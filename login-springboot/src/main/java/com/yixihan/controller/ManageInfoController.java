package com.yixihan.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yixihan.commons.Result;
import com.yixihan.pojo.ManageInfo;
import com.yixihan.pojo.ManageLogin;
import com.yixihan.pojo.UserInfo;
import com.yixihan.service.impl.ManageInfoServiceImpl;
import com.yixihan.service.impl.ManageLoginServiceImpl;
import com.yixihan.service.impl.UserInfoServiceImpl;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@RestController
@RequestMapping("/manage-info")
public class ManageInfoController {

    @Autowired
    private ManageInfoServiceImpl info;

    @Autowired
    private ManageLoginServiceImpl login;

    @Autowired
    private UserInfoServiceImpl userInfoService;


    @ApiOperation(value = "添加manage")
    @PostMapping("/add")
    public Result add (ManageInfo manage) {
        QueryWrapper<ManageInfo> wrapper1 = new QueryWrapper<>();
        QueryWrapper<ManageInfo> wrapper2 = new QueryWrapper<>();

        wrapper1.eq("Manage_name",manage.getManageName());
        wrapper2.eq("Manage_email",manage.getManageEmail());

        if ((info.getOne(wrapper1) == null) && (info.getOne(wrapper2) == null)) {
            info.save(manage);

            ManageLogin login = new ManageLogin(manage.getManagePassword());
            this.login.save(login);

            return Result.success(0,"注册成功",info.getById(manage.getManageId()));
        }
        else if (info.getOne(wrapper1) != null){
            return Result.fail(1,"该用户名已存在");
        }
        else if (info.getOne(wrapper2) != null) {
            return Result.fail(1,"该邮箱已存在");
        }
        else {
            return Result.fail(1,"注册失败");
        }
    }

    @ApiOperation(value = "删除manage")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="该用户的ID",required=true)
    })
    @PostMapping("/remove")
    public Result remove(@RequestParam int id) {
        boolean a = info.removeById(id);
        boolean b = this.login.removeById(id);

        if (a && b) {
            return Result.success(0,"删除成功");
        }
        else  {
            return Result.fail("删除失败");
        }
    }

    @ApiOperation(value = "更新manage资料")
    @PostMapping("/update")
    public Result update(ManageInfo manage) {
        boolean b = info.updateById(manage);

        if (b) {
            return Result.success(0,"修改成功");
        }
        else {
            return Result.fail("修改失败");
        }
    }

    @ApiOperation(value = "查看所有manage")
    @GetMapping("/list")
    public Result list() {
        return Result.success(0,"查询成功",info.list(null));
    }

    @ApiOperation(value = "根据ID查看manage")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="该用户的ID",required=true)
    })
    @GetMapping("/queryById")
    public Result queryById(@RequestParam int id) {
        ManageInfo one = info.getById(id);

        if (one == null) {
            return Result.fail("无此用户");
        }
        else {
            return Result.success(0,"查询成功",one);
        }
    }

    @ApiOperation(value = "根据Name查看manage")
    @ApiImplicitParams({
            @ApiImplicitParam(name="name",value="用户名",required=true)
    })
    @GetMapping("/queryByName")
    public Result queryByName(@RequestParam String name) {
        QueryWrapper<ManageInfo> wrapper = new QueryWrapper<>();

        wrapper.eq("Manage_name",name);

        ManageInfo one = info.getOne(wrapper);

        if (one == null) {
            return Result.fail("无此用户");
        }
        else {
            return Result.success(0,"查询成功",one);
        }
    }

    @ApiOperation(value = "根据Name审核用户", notes = "传过来是否审核通过的type和该用户的用户名")
    @ApiImplicitParams({
            @ApiImplicitParam(name="type",value="审核是否通过 1 通过, 2 不通过",required=true),
            @ApiImplicitParam(name="name",value="用户名",required=true),
            @ApiImplicitParam(name="AuditOpinion",value="审核意见")
    })
    @PostMapping("/examine1")
    public Result examine(@RequestParam int type, @RequestParam String name, @RequestParam String AuditOpinion) {
        QueryWrapper<UserInfo> wrapper1 = new QueryWrapper<>();
        QueryWrapper<UserInfo> wrapper2 = new QueryWrapper<>();

        wrapper1.eq("User_name",name);
        wrapper2.eq("Login_type",type);

        UserInfo one = userInfoService.getOne(wrapper1);
        one.setLoginType(type);
        one.setAuditOpinion(AuditOpinion);

        boolean b = userInfoService.updateById(one);

        if (b) {
            return Result.success(0,"OK",userInfoService.getById(one.getUserId()));
        }
        else return Result.fail("False");
    }

    @ApiOperation(value = "根据ID审核用户", notes = "传过来是否审核通过的type和该用户的ID")
    @ApiImplicitParams({
            @ApiImplicitParam(name="type",value="审核是否通过 1 通过, 2 不通过",required=true),
            @ApiImplicitParam(name="id",value="该用户的ID",required=true),
            @ApiImplicitParam(name="AuditOpinion",value="审核意见")
    })
    @PostMapping("/examine2")
    public Result examine(@RequestParam int type, @RequestParam int id, @RequestParam String AuditOpinion) {
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();

        wrapper.eq("Login_type",type);

        UserInfo one = userInfoService.getById(id);

        one.setLoginType(type);
        one.setAuditOpinion(AuditOpinion);

        boolean b = userInfoService.updateById(one);

        if (b) {
            return Result.success(0,"OK",userInfoService.getById(id));
        }
        else return Result.fail("False");
    }
}
