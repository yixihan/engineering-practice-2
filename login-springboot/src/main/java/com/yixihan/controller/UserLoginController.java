package com.yixihan.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yixihan.commons.Login;
import com.yixihan.commons.Result;
import com.yixihan.pojo.UserInfo;
import com.yixihan.pojo.UserLogin;
import com.yixihan.service.impl.UserInfoServiceImpl;
import com.yixihan.service.impl.UserLoginServiceImpl;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@RestController
@RequestMapping("/user-login")
public class UserLoginController {

    @Autowired
    private UserLoginServiceImpl login;

    @Autowired
    private UserInfoServiceImpl info;

    @ApiOperation(value = "用户登录")
    @PostMapping("/login")
    public Result Login (Login user) {
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();

        wrapper
                .eq("User_name",user.getName())
                .eq("User_password",user.getPassword());

        if(info.getOne(wrapper) != null) {
            return Result.success(0,"登录成功",user.getName());
        }
        else {
            return Result.fail("登录失败");
        }
    }

    @ApiOperation(value = "根据email找到该用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name="email",value="邮箱",required=true)
    })
    @PostMapping("/find")
    public Result FindUser(@RequestParam String email) {
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();

        wrapper.eq("User_email",email);

        UserInfo userInfo = info.getOne(wrapper);

        if(userInfo != null) {
            return Result.success(0,"查找成功",userInfo.getUserId());
        }
        else {
            return Result.fail("查找失败");
        }
    }

    @ApiOperation(value = "修改密码")
    @PostMapping("/change")
    public Result ChangePassword(UserLogin user) {
        UserInfo userInfo = new UserInfo(user.getUserId(), user.getUserPassword());
        boolean b = login.updateById(user);
        boolean c = info.updateById(userInfo);

        if (b && c) {
            return Result.success(0,"更改成功");
        }
        else {
            return Result.fail("更改失败");
        }
    }

}
