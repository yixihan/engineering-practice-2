package com.yixihan.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Api
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SuperLogin对象", description="")
public class SuperLogin implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "超管ID")
    @TableId(value = "Super_id", type = IdType.AUTO)
    @NotNull
    private Integer superId;

    @ApiModelProperty(value = "用户名")
    @TableField("Super_name")
    @NotNull
    private String superName;

    @ApiModelProperty(value = "密码")
    @TableField("Super_password")
    @NotNull
    private String superPassword;

    @ApiModelProperty(value = "逻辑删除")
    @TableField("Deleted")
    @TableLogic
    @NotNull
    private Integer Deleted;


}
