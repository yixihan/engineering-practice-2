package com.yixihan.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Api
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="UserInfo对象", description="")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户ID")
    @TableId(value = "User_id", type = IdType.AUTO)
    @NotNull
    private Integer userId;

    @ApiModelProperty(value = "用户名")
    @TableField("User_name")
    @NotNull
    private String userName;

    @ApiModelProperty(value = "密码")
    @NotNull
    private String userPassword;

    @ApiModelProperty(value = "联系电话")
    @TableField("User_tel")
    @NotNull
    private String userTel;

    @ApiModelProperty(value = "用户类型")
    @TableField("User_type")
    @NotNull
    private String userType;

    @ApiModelProperty(value = "姓名")
    @TableField("name")
    @NotNull
    private String name;

    @ApiModelProperty(value = "部门")
    @TableField("User_dep")
    @NotNull
    private String UserDep;

    @ApiModelProperty(value = "企业类型(1代表供应商,2代表采购商)")
    @TableField("User_firm_type")
    @NotNull
    private Integer UserFirmType;

    @ApiModelProperty(value = "公司名字")
    @TableField("Company_name")
    @NotNull
    private String companyName;

    @ApiModelProperty(value = "法人代表")
    @TableField("User_JP")
    @NotNull
    private String userJp;

    @ApiModelProperty(value = "法人身份证")
    @TableField("User_JP_IDcard")
    @NotNull
    private String userJpIdcard;

    @ApiModelProperty(value = "注册地区")
    @TableField("User_address")
    private String userAddress;

    @ApiModelProperty(value = "企业邮箱")
    @TableField("User_email")
    @NotNull
    private String userEmail;

    @ApiModelProperty(value = "传真")
    @TableField("User_fax")
    private String userFax;

    @ApiModelProperty(value = "邮政编码")
    @TableField("Post_code")
    private Integer postCode;

    @ApiModelProperty(value = "注册资金（万元）")
    @TableField("User_fund")
    private Integer userFund;

    @ApiModelProperty(value = "营业执照号")
    @TableField("User_work_ID")
    private String userWorkId;

    @ApiModelProperty(value = "组织机构代码")
    @TableField("User_gruop_ID")
    private String userGruopId;

    @ApiModelProperty(value = "经营许可证编号")
    @TableField("User_operation_ID")
    private String userOperationId;

    @ApiModelProperty(value = "税务登记证代码")
    @TableField("User_Tax_ID")
    private String userTaxId;

    @ApiModelProperty(value = "开户银行")
    @TableField("User_bank")
    private String userBank;

    @ApiModelProperty(value = "（银行）账号")
    @TableField("User_bank_ID")
    private String userBankId;

    @ApiModelProperty(value = "煤炭存放地点、数量、质量")
    @TableField("User_number")
    private String userNumber;

    @ApiModelProperty(value = "运输方式及保障能力")
    @TableField("User_tram")
    private String userTram;

    @ApiModelProperty(value = "应商介绍")
    @TableField("User_synopsis")
    private String userSynopsis;

    @ApiModelProperty(value = "审核建议")
    @TableField("Audit_opinion")
    private String AuditOpinion;

    @ApiModelProperty(value = "注册状态")
    @TableField("Login_type")
    @NotNull
    private Integer loginType;

    @ApiModelProperty(value = "逻辑删除")
    @TableField("Deleted")
    @TableLogic
    @NotNull
    private Integer Deleted;


    public UserInfo(Integer userId, String userPassword) {
        this.userId = userId;
        this.userPassword = userPassword;
    }
}
