package com.yixihan.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Api
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ManageInfo对象", description="")
public class ManageInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "平台管理人员ID")
    @TableId(value = "Manage_id", type = IdType.AUTO)
    @NotNull
    private Integer manageId;

    @ApiModelProperty(value = "用户名")
    @TableField("Manage_name")
    @NotNull
    private String manageName;

    @ApiModelProperty(value = "密码")
    @TableField("Manage_password")
    @NotNull
    private String managePassword;

    @ApiModelProperty(value = "邮箱")
    @TableField("Manage_email")
    @NotNull
    private String manageEmail;

    @ApiModelProperty(value = "联系方式")
    @TableField("Manage_tel")
    @NotNull
    private String manageTel;

    @ApiModelProperty(value = "管理人员类型（角色）")
    @TableField("Manage_character")
    @NotNull
    private String manageCharacter;

    @ApiModelProperty(value = "逻辑删除")
    @TableField("Deleted")
    @TableLogic
    @NotNull
    private Integer Deleted;


    public ManageInfo(String managePassword) {
        this.managePassword = managePassword;
    }
}
