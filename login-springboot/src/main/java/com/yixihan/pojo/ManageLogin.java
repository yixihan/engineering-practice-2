package com.yixihan.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Api
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ManageLogin对象", description="")
public class ManageLogin implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "平台管理人员ID")
    @TableId(value = "Manage_id", type = IdType.AUTO)
    private Integer manageId;

    @ApiModelProperty(value = "密码")
    @TableField("Manage_password")
    private String managePassword;

    @ApiModelProperty(value = "逻辑删除")
    @TableField("Deleted")
    @TableLogic
    private Integer Deleted;


    public ManageLogin(String managePassword) {
        this.managePassword = managePassword;
    }
}
