package com.yixihan.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Api
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="UserImg对象", description="")
public class UserImg implements Serializable {

    @ApiModelProperty(value = "用户ID")
    @TableId(value = "User_id")
    @NotNull
    private Integer userId;

    @ApiModelProperty(value = "营业执照")
    @TableField("User_operation_name")
    private String userOperationName;

    @ApiModelProperty(value = "营业执照保存地址")
    @TableField("User_operation_url")
    private String userOperationUrl;

    @ApiModelProperty(value = "税务登记证")
    @TableField("User_Tax_name")
    private String userTaxName;

    @ApiModelProperty(value = "税务登记证保存地址")
    @TableField("User_Tax_url")
    private String userTaxUrl;

    @ApiModelProperty(value = "组织机构代码证")
    @TableField("User_gruop_name")
    private String userGruopName;

    @ApiModelProperty(value = "组织机构代码证保存地址")
    @TableField("User_gruop_url")
    private String userGruopUrl;

    @ApiModelProperty(value = "开户银行")
    @TableField("User_bank_name")
    private String userBankName;

    @ApiModelProperty(value = "开户银行保存地址")
    @TableField("User_bank_url")
    private String userBankUrl;

    @ApiModelProperty(value = "煤炭经营许可证")
    @TableField("User_coal_name")
    private String userCoalName;

    @ApiModelProperty(value = "煤炭经营许可证保存地址")
    @TableField("User_coal_url")
    private String userCoalUrl;

    @ApiModelProperty(value = "法人身份证")
    @TableField("User_JP_name")
    private String userJpName;

    @ApiModelProperty(value = "法人身份证保存地址")
    @TableField("User_JP_url")
    private String userJpUrl;

    @ApiModelProperty(value = "逻辑删除")
    @TableField("Deleted")
    @TableLogic
    private Integer Deleted;


    public UserImg(Integer userId) {
        this.userId = userId;
    }
}
