package com.yixihan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableAsync
@EnableCaching
@EnableWebMvc
@SpringBootApplication
public class LoginSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoginSpringbootApplication.class, args);
    }

}
