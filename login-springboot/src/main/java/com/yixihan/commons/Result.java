package com.yixihan.commons;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    private int status; //0代表成功
    private String msg;
    private Object data;//返回数据

    //成功
    public static Result success( Object data){
        return success(1,"操作成功",data);
    }

    //成功
    public static Result success(int status,String msg,Object data){
        return new Result(status,msg,data);
    }

    //成功
    public static Result success(int status, String msg) {
        return success(status,msg,null);
    }

    //失败
    public static Result fail(String msg){

        return fail(1,msg,null);
    }

    //失败
    public static Result fail(String msg,Object data){

        return fail(1,msg,data);
    }

    //失败
    public static Result fail(int status,String msg){

        return fail(status,msg,null);
    }

    //失败
    public static Result fail(int status,String msg,Object data){
        return new Result(status,msg,data);
    }
}
