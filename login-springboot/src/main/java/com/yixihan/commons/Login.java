package com.yixihan.commons;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Api
@ApiModel(value = "登录接口")
public class Login {

    @ApiModelProperty(value = "用户名")
    @NotNull
    private String name;

    @ApiModelProperty(value = "密码")
    @NotNull
    private String password;
}
