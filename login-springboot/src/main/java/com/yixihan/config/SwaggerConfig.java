package com.yixihan.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
//开启 swagger2
@EnableSwagger2
public class SwaggerConfig {

    //配置了 Swagger 的 Docket 的 bean 实例
    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo());
    }

    //配置 Swagger 的信息 apiInfo
    private ApiInfo apiInfo() {

        //作者信息
        Contact DEFAULT_CONTACT = new Contact("易曦翰", "https://gitee.com/yixihan", "3113788997@qq.com");

        return new ApiInfo(
                "易曦翰的 Swagger Api 文档",
                "花开亦花散",
                "1.0",
                "https://gitee.com/yixihan/engineering-practice-2",
                DEFAULT_CONTACT,
                "Apache 2.0",
                "https://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList<>()
        );
    }
}