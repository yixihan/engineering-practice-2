package com.yixihan.config;

import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.injector.LogicSqlInjector;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@MapperScan("com.yixihan")
@Configuration
@EnableTransactionManagement
public class MybatisPlusConfig {
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    //逻辑删除组件
    @Bean
    public ISqlInjector sqlInjector() {
        return new LogicSqlInjector();
    }

    //sql 执行效率插件
    @Bean
    public PerformanceInterceptor performanceInterceptor() {
        PerformanceInterceptor paginationInterceptor = new PerformanceInterceptor();

        //ms 设置 sql 执行的最大时间,如果超过了则不执行
        paginationInterceptor.setMaxTime(1000);
        //设置格式化
        paginationInterceptor.setFormat(true);
        return paginationInterceptor;
    }
}

