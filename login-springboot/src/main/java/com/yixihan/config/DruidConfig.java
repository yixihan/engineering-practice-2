package com.yixihan.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import javax.sql.DataSource;
import java.util.HashMap;


@Configuration
public class DruidConfig {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource druidDataSource () {
        return new DruidDataSource();
    }

    //后台监控功能

    //ServletRegistrationBean 相当于 web.xml
    //因为 SpringBoot 内置了 Servlet 容器，所以没有 web.xml ，替代方法， ServletRegistrationBean
    @Bean
    public ServletRegistrationBean servletRegistrationBean() {
        ServletRegistrationBean<StatViewServlet> bean =
                new ServletRegistrationBean<>(new StatViewServlet(), "/druid/*");

        //后台需要有人登录，账号密码配置

        HashMap<String, String> initParameters = new HashMap<>();

        //增加配置 登录 key 是固定的  loginUsername  loginPassword
        initParameters.put("loginUsername", "admin");
        initParameters.put("loginPassword", "admin");

        //允许谁可以访问
        initParameters.put("allow", "");    //参数为空，代表所有人都能访问



        bean.setInitParameters(initParameters);   //初始化参数

        return bean;

    }

    //filter
    @Bean
    public FilterRegistrationBean filterRegistrationBean () {
        FilterRegistrationBean<Filter> bean = new FilterRegistrationBean<>();

        bean.setFilter(new WebStatFilter());

        //可以过滤哪些请求

        HashMap<String, String> map = new HashMap<>();

        //这些东西不进行统计
        map.put("exclusions", "*.js, *.css, /druid/*");

        bean.setInitParameters(map);

        return bean;
    }

}
