package com.yixihan.mapper;

import com.yixihan.pojo.ManageLogin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 易曦翰
 * @since 2021-08-05
 */
public interface ManageLoginMapper extends BaseMapper<ManageLogin> {

}
